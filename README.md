Pending tasks:
1. Add validation to model
2. Improve error handling
3. Add log
4. Use TestNG instead of Junit
5. Remove duplicate code from tests
6. Add comment to the code when it is required