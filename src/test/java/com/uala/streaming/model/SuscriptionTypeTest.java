package com.uala.streaming.model;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class SuscriptionTypeTest {

    private StreamingPlatform platform = new StreamingPlatformCreator().create();

    @Test(expected =  IllegalArgumentException.class)
    public void classicSuscription(){
        User user = create(SuscriptionType.classic);
        platform.login(user);
        try {
            platform.login(user);
            Assert.assertTrue( user.getActiveSessions() == 1);
        }catch ( IllegalArgumentException e) {
            Assert.assertTrue( user.getActiveSessions() == user.getSuscription().getType().getAllowedConcurrentLogins());
            Assert.assertEquals(e.getMessage(), "The limit of concurrent session has been reached");
            throw e;
        }
    }

    @Test(expected =  IllegalArgumentException.class)
    public void goldSubscription(){
        User user = create(SuscriptionType.gold);
        platform.login(user);
        Assert.assertTrue( user.getActiveSessions() == 1);
        platform.login(user);
        Assert.assertTrue( user.getActiveSessions() == 2);
        platform.login(user);
        Assert.assertTrue( user.getActiveSessions() == 3);
        try {
            platform.login(user);
        }catch ( IllegalArgumentException e) {
            Assert.assertEquals(e.getMessage(), "The limit of concurrent session has been reached");
            Assert.assertTrue( user.getActiveSessions() == user.getSuscription().getType().getAllowedConcurrentLogins());
            throw e;
        }
    }

    @Test(expected =  IllegalArgumentException.class)
    public void platinumSubscription(){
        User user = create(SuscriptionType.platinum);
        platform.login(user);
        platform.login(user);
        platform.login(user);
        platform.login(user);
        platform.login(user);
        Assert.assertTrue( user.getActiveSessions() == 5);
        try {
            platform.login(user);
        }catch ( IllegalArgumentException e) {
            Assert.assertEquals(e.getMessage(), "The limit of concurrent session has been reached");
            Assert.assertTrue( user.getActiveSessions() == user.getSuscription().getType().getAllowedConcurrentLogins());
            throw e;
        }
    }

    @Test
    public void classicWithLogoutSubscription(){
        User user = create(SuscriptionType.classic);
        platform.login(user);
        Assert.assertTrue( user.getActiveSessions() == 1);
        platform.logout(user);
        Assert.assertTrue( user.getActiveSessions() == 0);
    }

    private User create(SuscriptionType type){
        return new User("user with "+type, new Suscription(type, BigDecimal.valueOf(100)), Mood.happy, PaymentType.creditCard);
    }
}
