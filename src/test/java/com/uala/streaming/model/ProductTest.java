package com.uala.streaming.model;

import static  org.junit.Assert.assertTrue;
import static  org.junit.Assert.assertFalse;
import org.junit.Test;

import java.util.Date;

public class ProductTest {

    @Test
    public void interestingSeries(){
        assertTrue(new Serie(5).isInteresting());
        assertTrue(new Serie(4).isInteresting());
        assertFalse(new Serie(1).isInteresting());
        assertFalse(new Serie(2).isInteresting());
        assertFalse(new Serie(3).isInteresting());
    }

    @Test
    public void interestingFilms(){
        assertFalse(new Film(0, new Date(), 12 ).isInteresting());
        assertTrue(new Film(1, new Date(), 12 ).isInteresting());
        assertTrue(new Film(2, new Date(), 12 ).isInteresting());
        assertTrue(new Film(3, new Date(), 12 ).isInteresting());
    }

    @Test
    public void interestingDocumental(){
        assertFalse(new Documentary("not interesting").isInteresting());
        assertFalse(new Documentary("un official").isInteresting());
        assertFalse(new Documentary("unoficial").isInteresting());
        assertTrue(new Documentary("unofficial").isInteresting());
    }
}