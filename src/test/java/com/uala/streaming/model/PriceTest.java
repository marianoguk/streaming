package com.uala.streaming.model;

import org.junit.Test;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

public class PriceTest {
    private StreamingPlatformCreator creator = new StreamingPlatformCreator();

    @Test
    public void withPaypalAndTwoYearsSuscriptions(){
        User user = create(PaymentType.paypal);
        StreamingPlatform p = creator.create();
        updateSuscription(user, 2);
        assertTrue( 95 == p.calculatePrice(user).longValue());
    }

    @Test
    public void withPaypalAndFourYearsSuscriptions(){
        User user = create(PaymentType.paypal);
        StreamingPlatform p = creator.create();
        updateSuscription(user, 4);
        assertTrue(89 == p.calculatePrice(user).longValue());
    }

    @Test
    public void withPaypalAndOneYearsSuscriptions(){
        User user = create(PaymentType.paypal);
        StreamingPlatform p = creator.create();
        updateSuscription(user, 1);
        assertTrue(98 == p.calculatePrice(user).longValue());
    }

    @Test
    public void withPaypalAndZeroYearsSuscriptions(){
        User user = create(PaymentType.paypal);
        StreamingPlatform p = creator.create();
        assertTrue(98 == p.calculatePrice(user).longValue());
    }


    // Without Payment method discount
    @Test
    public void withoutPaypalAndTwoYearsSuscriptions(){
        User user = create(PaymentType.creditCard);
        StreamingPlatform p = creator.create();
        updateSuscription(user, 2);
        assertTrue(97 == p.calculatePrice(user).longValue());
    }

    @Test
    public void withoutPaypalAndFourYearsSuscriptions(){
        User user = create(PaymentType.creditCard);
        StreamingPlatform p = creator.create();
        updateSuscription(user, 4);
        assertTrue(91 == p.calculatePrice(user).longValue());
    }

    @Test
    public void withoutPaypalAndZeroYearsSuscriptions(){
        User user = create(PaymentType.creditCard);
        StreamingPlatform p = creator.create();
        updateSuscription(user, 0);
        assertTrue(100 == p.calculatePrice(user).longValue());
    }

    @Test
    public void withoutPaypalAndOneYearsSuscriptions(){
        User user = create(PaymentType.creditCard);
        StreamingPlatform p = creator.create();
        updateSuscription(user, 1);
        assertTrue(100 == p.calculatePrice(user).longValue());
    }

    private void updateSuscription(User user, int yearsAgo){
        user.updateSuscription(DateUtilsForTest.minusYears(yearsAgo));
    }

    private User create(PaymentType type){
        return new User("user with "+type, new Suscription(SuscriptionType.classic, BigDecimal.valueOf(100)), Mood.happy, type);
    }
}