package com.uala.streaming.model;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.*;

public class RecommendationTest {

    private StreamingPlatformCreator creator = new StreamingPlatformCreator();

    @Test
    public void sadMoodRecommendations() {
        StreamingPlatform p = creator.create();
        p.add(new Film(1, new Date(), 90));
        p.add(new Film(1, new Date(), 119));
        p.add(new Film(1, new Date(), 120));
        p.add(new Film(1, new Date(), 121));
        p.add(new Film(1, new Date(), 333));
        p.add(new Film(1, new Date(), 64));

        User user = new User("sad", new Suscription (SuscriptionType.platinum, BigDecimal.valueOf(10)), Mood.sad, PaymentType.paypal);

        Assert.assertTrue(p.getRecommendations(user).size() == 2);
    }

    @Test
    public void melancholicMoodRecommendations() {
        StreamingPlatform p = creator.create();
        p.add(new Film(1, create(0), 90));
        p.add(new Film(1, create(9), 90));
        p.add(new Film(1, create(10), 90));
        p.add(new Film(1, create(11), 90));
        p.add(new Film(1, create(12), 90));

        User user = new User("melancholic", new Suscription (SuscriptionType.platinum, BigDecimal.valueOf(10)), Mood.melancholic, PaymentType.paypal);

        Assert.assertTrue(p.getRecommendations(user).size() == 2);
    }

    private Date create(int yearsAgo){
        return DateUtilsForTest.minusYears(yearsAgo);
    }

    @Test
    public void happyMoodRecommendations(){
        StreamingPlatform p = creator.create();
        p.add(new Film(1, new Date(), 90));
        p.add(new Film(1, new Date(), 119));
        p.add(new Film(1, new Date(), 120));
        p.add(new Film(1, new Date(), 121));
        p.add(new Film(1, new Date(), 333));
        p.add(new Film(1, new Date(), 64));

        p.add(new Film(1, create(0), 90));
        p.add(new Film(1, create(9), 90));
        p.add(new Film(1, create(10), 90));
        p.add(new Film(1, create(11), 90));
        p.add(new Film(1, create(12), 90));

        p.add(new Film(1, create(0), 190));
        p.add(new Film(1, create(9), 90));
        p.add(new Film(1, create(10), 90));
        p.add(new Film(1, create(11), 90));
        p.add(new Film(1, create(12), 290));

        User user = new User("happy", new Suscription (SuscriptionType.platinum, BigDecimal.valueOf(10)), Mood.happy, PaymentType.paypal);

        Assert.assertTrue(p.getRecommendations(user).size() == 16);
    }


}
