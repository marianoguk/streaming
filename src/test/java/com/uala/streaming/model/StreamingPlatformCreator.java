package com.uala.streaming.model;

import java.math.BigDecimal;

public class StreamingPlatformCreator {

    public StreamingPlatform create(){
        StreamingPlatform platform = new StreamingPlatform(discountEngine());
        Suscription classic = new Suscription(SuscriptionType.classic, BigDecimal.valueOf(10));
        Suscription gold = new Suscription(SuscriptionType.gold, BigDecimal.valueOf(100));
        Suscription plat = new Suscription(SuscriptionType.platinum, BigDecimal.valueOf(1000));
        platform
            .add(createUser(classic, PaymentType.paypal, Mood.happy))
            .add(createUser(gold, PaymentType.paypal, Mood.sad))
            .add(createUser(plat, PaymentType.paypal, Mood.melancholic));

        platform.add(classic).add(gold).add(plat);
        return platform;
    }

    private DiscountEngine discountEngine(){
        return new DiscountEngine()
                .add( new PaymentMethodDiscount())
                .add(new FidelityDiscount());
    }

    private User createUser(Suscription s,  PaymentType paymentType, Mood mood){
        return new User("user"+s.getType(), s, mood, paymentType);
    }
}
