package com.uala.streaming.model;

import java.util.Calendar;
import java.util.Date;

public final class DateUtilsForTest {

    public static Date minusYears(int yearsAgo){
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        Calendar date = Calendar.getInstance();
        date.set(Calendar.YEAR, currentYear - yearsAgo);
        return date.getTime();
    }
}
