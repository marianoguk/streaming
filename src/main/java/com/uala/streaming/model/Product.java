package com.uala.streaming.model;

import java.util.Date;

public abstract class Product {
    private ProductType type;

    protected Product (ProductType type){
        this.type = type;
    }

   public abstract boolean isInteresting();

    public ProductType getType() {
        return type;
    }
}
