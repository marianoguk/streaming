package com.uala.streaming.model;

public class Serie extends Product {
    private int seasons;

    public Serie(int seasons){
        super(ProductType.serie);
        this.seasons = seasons;
    }

    @Override
    public boolean isInteresting() {
        return seasons == 4 || seasons == 5;
    }
}
