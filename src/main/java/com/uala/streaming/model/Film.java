package com.uala.streaming.model;

import java.util.Date;

public class Film extends Product {
    private static int LOWER_OSCAR_LIMIT_TO_BE_INTERESTING = 1;
    private int oscars;
    private Date editionDate;
    private int durationInMinutes;

    public Film(int oscars, Date editionDate, int durationInMinutes) {
        super(ProductType.film);
        this.oscars = oscars;
        this.editionDate = editionDate;
        this.durationInMinutes = durationInMinutes;
    }

    @Override
    public boolean isInteresting() {
        return oscars >= LOWER_OSCAR_LIMIT_TO_BE_INTERESTING;
    }

    public Date getEditionDate() {
        return editionDate;
    }

    public int getOscars() {
        return oscars;
    }

    public int getDurationInMinutes() {
        return durationInMinutes;
    }
}
