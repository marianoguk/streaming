package com.uala.streaming.model;

import java.util.ArrayList;
import java.util.List;

public class DiscountEngine {
    private List<DiscountPolicy> policies = new ArrayList<>();

    public DiscountEngine(){

    }

    public long apply(User user){
        long totalDiscount = 0;
        for (DiscountPolicy d : policies){
            totalDiscount += d.apply(user);
        }
        return totalDiscount;
    }

    public DiscountEngine add (DiscountPolicy policy){
        policies.add(policy);
        return this;
    }
}
