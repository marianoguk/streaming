package com.uala.streaming.model;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public final class DateUtils {

    public static long yearsOfDifference(Date first, Date last){
        LocalDate start = new java.sql.Date(first.getTime()).toLocalDate();
        LocalDate end = new java.sql.Date(last.getTime()).toLocalDate();
        return ChronoUnit.YEARS.between(start, end);
    }

}
