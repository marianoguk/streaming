package com.uala.streaming.model;

import java.util.Date;

public class FidelityDiscount implements  DiscountPolicy {
    private long FIDELITY_DISCOUNT_PER_YEAR = 3;
    @Override
    public long apply(User user) {
        long diff = Math.max(0, DateUtils.yearsOfDifference(user.getSubscriptionDate(), new Date())-1);
        return FIDELITY_DISCOUNT_PER_YEAR * diff ;
    }
}
