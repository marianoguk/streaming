package com.uala.streaming.model;

import java.math.BigDecimal;

public class Suscription {
    private SuscriptionType type;
    private BigDecimal price;

    public Suscription(SuscriptionType type, BigDecimal price) {
        this.type = type;
        this.price = price;
    }

    public SuscriptionType getType() {
        return type;
    }

    public BigDecimal getPrice() {
        return price;
    }
}
