package com.uala.streaming.model;

public enum SuscriptionType {
    classic(1), gold(3), platinum(5);

    private int allowedConcurrentLogins;

    SuscriptionType(int concurrentLogins) {
        this.allowedConcurrentLogins = concurrentLogins;
    }

    public int getAllowedConcurrentLogins() {
        return allowedConcurrentLogins;
    }
}
