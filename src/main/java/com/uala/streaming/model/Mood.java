package com.uala.streaming.model;

import java.sql.Date;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;


public enum Mood {
    sad{
        @Override
        public boolean accept(User user, Film film) {
            return film.getDurationInMinutes() > 2*60;
        }
    }, happy,
    melancholic {
        @Override
        public boolean accept(User user, Film film) {
            LocalDate published = new Date(film.getEditionDate().getTime()).toLocalDate();
            return ChronoUnit.YEARS.between(published, LocalDate.now()) > 10;
        }
    };

    public boolean accept(User user, Film film){
        return true;
    }
}
