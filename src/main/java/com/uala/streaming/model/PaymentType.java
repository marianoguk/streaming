package com.uala.streaming.model;

public enum PaymentType {
    creditCard, paypal;
}
