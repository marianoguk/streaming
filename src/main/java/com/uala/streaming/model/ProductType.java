package com.uala.streaming.model;

public enum ProductType {
    serie, film, documentary
}
