package com.uala.streaming.model;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

public class StreamingPlatform {
    private Map<ProductType, List<Product>> catalog;
    private Set<User> users;
    private Set<Suscription> subscriptions;
    private DiscountEngine discountEngine ;

    public StreamingPlatform(DiscountEngine discountEngine ){
        this.catalog = new HashMap<>();
        this.users = new HashSet<>();
        this.subscriptions = new HashSet<>();
        this.discountEngine = discountEngine;
    }

    public StreamingPlatform add (Product p){
        List<Product> products = catalog.get(p.getType());
        if(products == null){
            products = new ArrayList<>();
            catalog.put(p.getType(), products);
        }
        products.add(p);
        return this;
    }

    public StreamingPlatform add(User user){
        users.add(user);
        return this;
    }

    public StreamingPlatform add (Suscription suscription){
        subscriptions.add(suscription);
        return this;
    }

    public List<Product> getRecommendations(User user) {
        List<Product> films = catalog.get(ProductType.film);
        return films.parallelStream()
                .filter( it -> user.isInterestedIn((Film)it))
                .collect(Collectors.toList());
    }

    public BigDecimal calculatePrice(User user){
        BigDecimal discount = BigDecimal.valueOf(100).subtract(BigDecimal.valueOf(discountEngine.apply(user))).divide(BigDecimal.valueOf(100));
        return user.getSuscription().getPrice().multiply(discount);
    }

    public void login (User user){
        user.login();
    }
    public void logout(User user){
        user.logout();
    }

}
