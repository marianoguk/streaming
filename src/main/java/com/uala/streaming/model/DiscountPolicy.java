package com.uala.streaming.model;

public interface DiscountPolicy {
    long apply(User user);
}
