package com.uala.streaming.model;

public class Documentary extends Product{
    private static final String INTERESTING_TITLE_PATTERN= "unofficial";
    private String title;


    public Documentary(String title) {
        super(ProductType.documentary);
        if(title == null || title.isEmpty()) {
            throw new IllegalArgumentException("Title cannot be null or empty");
        }
        this.title = title;
    }

    @Override
    public boolean isInteresting() {
        return title.contains(INTERESTING_TITLE_PATTERN);
    }
}
