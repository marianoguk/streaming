package com.uala.streaming.model;

public class PaymentMethodDiscount implements DiscountPolicy {
    private static final long PAYPAL_DISCOUNT = 2;

    @Override
    public long apply(User user) {
        return PaymentType.paypal ==  user.getPaymentType() ? PAYPAL_DISCOUNT : 0;
    }
}
