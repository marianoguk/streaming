package com.uala.streaming.model;

import java.util.Date;

public class User {
    private String username;
    private Suscription suscription;
    private Mood mood;
    private Date subscriptionDate;
    private PaymentType paymentType;
    private int sessions = 0;

    public User(String username, Suscription suscription, Mood mood, PaymentType paymentType) {
        this.username = username;
        this.suscription = suscription;
        this.mood = mood;
        this.subscriptionDate = new Date();
        this.paymentType = paymentType;
    }

    public Suscription getSuscription() {
        return suscription;
    }

    public Date getSubscriptionDate() {
        return subscriptionDate;
    }

    public String getUsername() {
        return username;
    }

    public boolean isInterestedIn(Film film){
        return mood.accept(this, film);
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void updateSuscription(Date newDate){
        this.subscriptionDate = newDate;
    }

    public synchronized int getActiveSessions(){
        return sessions;
    }
    public synchronized void login(){
        if(suscription.getType().getAllowedConcurrentLogins() < sessions +1 ) {
            throw  new IllegalArgumentException("The limit of concurrent session has been reached");
        }
        sessions++;
    }

    public synchronized  void logout (){
        if(sessions -1 <0 ){
            throw  new IllegalArgumentException("No session has been started");
        }
        sessions--;
    }

    @Override
    public int hashCode() {
        return username.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        return  ((User)o).getUsername().equals(username) ;
    }
}
